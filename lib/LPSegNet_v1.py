import torch
import torch.nn             as nn
import torch.nn.functional  as F

from .Res2Net_v1b   import res2net50_v1b_26w_4s
from torch.autograd import Variable

class BasicConv2d(nn.Module):
    def __init__(self, in_planes, out_planes, kernel_size, stride=1, padding=0, dilation=1):
        super(BasicConv2d, self).__init__()
        self.conv   = nn.Conv2d(in_planes, out_planes,
                              kernel_size=kernel_size, stride=stride,
                              padding=padding, dilation=dilation, bias=False)
        self.bn     = nn.BatchNorm2d(out_planes)
        self.relu   = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        return x


class RFB_modified(nn.Module):
    def __init__(self, in_channel, out_channel):
        super(RFB_modified, self).__init__()
        self.relu = nn.ReLU(True)
        self.branch0 = nn.Sequential(
            BasicConv2d(in_channel, out_channel, 1),
        )
        self.branch1 = nn.Sequential(
            BasicConv2d(in_channel, out_channel, 1),
            BasicConv2d(out_channel, out_channel, kernel_size=(1, 3), padding=(0, 1)),
            BasicConv2d(out_channel, out_channel, kernel_size=(3, 1), padding=(1, 0)),
            BasicConv2d(out_channel, out_channel, 3, padding=3, dilation=3)
        )
        self.branch2 = nn.Sequential(
            BasicConv2d(in_channel, out_channel, 1),
            BasicConv2d(out_channel, out_channel, kernel_size=(1, 5), padding=(0, 2)),
            BasicConv2d(out_channel, out_channel, kernel_size=(5, 1), padding=(2, 0)),
            BasicConv2d(out_channel, out_channel, 3, padding=5, dilation=5)
        )
        self.branch3 = nn.Sequential(
            BasicConv2d(in_channel, out_channel, 1),
            BasicConv2d(out_channel, out_channel, kernel_size=(1, 7), padding=(0, 3)),
            BasicConv2d(out_channel, out_channel, kernel_size=(7, 1), padding=(3, 0)),
            BasicConv2d(out_channel, out_channel, 3, padding=7, dilation=7)
        )
        self.conv_cat = BasicConv2d(4*out_channel, out_channel, 3, padding=1)
        self.conv_res = BasicConv2d(in_channel, out_channel, 1)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        x_cat = self.conv_cat(torch.cat((x0, x1, x2, x3), 1))
        x = self.relu(x_cat + self.conv_res(x))
        
        return x

class aggregation(nn.Module):

    def __init__(self, channel):
        super(aggregation, self).__init__()
        self.relu = nn.ReLU(True)

        self.upsample = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        self.conv_upsample1 = BasicConv2d(channel, channel, 3, padding=1)
        self.conv_upsample2 = BasicConv2d(channel, channel, 3, padding=1)
        self.conv_upsample3 = BasicConv2d(channel, channel, 3, padding=1)
        self.conv_upsample4 = BasicConv2d(channel, channel, 3, padding=1)
        self.conv_upsample5 = BasicConv2d(2*channel, 2*channel, 3, padding=1)

        self.conv_concat2   = BasicConv2d(2*channel, 2*channel, 3, padding=1)
        self.conv_concat3   = BasicConv2d(3*channel, 3*channel, 3, padding=1)
        self.conv4          = BasicConv2d(3*channel, 3*channel, 3, padding=1)
        self.conv5          = nn.Conv2d(3*channel, 1, 1)

    def forward(self, x1, x2, x3):
        x1_1 = x1
        x2_1 = self.conv_upsample1(self.upsample(x1)) * x2
        x3_1 = self.conv_upsample2(self.upsample(self.upsample(x1))) \
               * self.conv_upsample3(self.upsample(x2)) * x3

        x2_2 = torch.cat((x2_1, self.conv_upsample4(self.upsample(x1_1))), 1)
        x2_2 = self.conv_concat2(x2_2)

        x3_2 = torch.cat((x3_1, self.conv_upsample5(self.upsample(x2_2))), 1)
        x3_2 = self.conv_concat3(x3_2)

        x    = self.conv4(x3_2)
        x    = self.conv5(x)
        return x

class aggregation_LP(nn.Module):
    def __init__(self, channel):
        super(aggregation_LP, self).__init__()
        self.upsample = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        self.conv_upsample1 = BasicConv2d(channel, channel, 3, padding=1)
        self.conv4          = BasicConv2d(channel, channel, 3, padding=1)
        self.conv5          = nn.Conv2d(channel, 1, 1)

    def forward(self, x1):
        x1_ = self.conv_upsample1(self.upsample(x1))
        x    = self.conv4(x1_)
        x    = self.conv5(x)
        return x

class LPSegNet(nn.Module):
    # res2net based encoder decoder
    def __init__(self, channel=32, use_attention='PCM', mode_cls = 'max_pooling'):
        super(LPSegNet, self).__init__()
        self.use_attention = use_attention
        self.mode_cls      = mode_cls
        self.resnet        = res2net50_v1b_26w_4s(pretrained=True)

        # ---- Receptive Field Block like module ----
        self.rfb2_1 = RFB_modified(512, channel)
        self.rfb3_1 = RFB_modified(1024, channel)
        self.rfb4_1 = RFB_modified(2048, channel)

        # ---- Partial Decoder ----
        self.agg1   = aggregation(channel)
        self.agg_LP = aggregation_LP(channel)

    def forward(self, x):
        x = self.resnet.conv1(x)
        x = self.resnet.bn1(x)
        x = self.resnet.relu(x)
        x = self.resnet.maxpool(x)      

        x1 = self.resnet.layer1(x)      
        x2 = self.resnet.layer2(x1)     
        x3 = self.resnet.layer3(x2)     
        x4 = self.resnet.layer4(x3)     

        # ---- RFB ------
        value = self.rfb2_1(x2)        
        key   = self.rfb3_1(x3)       
        query = self.rfb4_1(x4)        

        ra5_feat = self.agg1(query, key, value)
        lateral_map_5 = F.interpolate(ra5_feat, scale_factor=8, mode='bilinear')    # NOTES: Sup-1 (bs, 1, 44, 44) -> (bs, 1, 352, 352)

        if self.use_attention == 'PCM':

            n,c,h,w = ra5_feat.size()
            x_      = F.interpolate(x,(h,w),mode='bilinear',align_corners=True)
            key_    = F.interpolate(key,(h,w),mode='bilinear',align_corners=True)
            query_  = F.interpolate(query,(h,w),mode='bilinear',align_corners=True)
            head    = torch.cat([x_, key_, query_], dim = 1)

            pcm, attention = self.PCM(value, head)
            pcm = self.agg_LP(pcm)
            pcm_ = F.interpolate(pcm, scale_factor=4, mode='bilinear')    # NOTES: Sup-1 (bs, 1, 44, 44) -> (bs, 1, 352, 352)
      
        if self.mode_cls == 'max_pooling':
            B,C,W,H = pcm_.shape
            max_pool = nn.MaxPool2d((W, H))
            classifcation = max_pool(torch.sigmoid(pcm_))

        return lateral_map_5, pcm_, classifcation

    def PCM(self, value, attention):
        
        n,c,h,w = attention.size()
        
        proj_value = F.interpolate(value, (h,w), mode='bilinear', align_corners=True).view(n,-1,h*w)
        # attention_ = attention
        attention = attention.view(n,-1,h*w)
        attention = attention/(torch.norm(attention,dim=1,keepdim=True)+1e-5)

        attention_matrix = torch.matmul(attention.transpose(1,2), attention)
        attention_matrix = attention_matrix/(torch.sum(attention_matrix,dim=1,keepdim=True)+1e-5)
        
        output = torch.matmul(proj_value, attention_matrix).view(n,-1,h,w)
        output = output
        
        return output

if __name__ == '__main__':
    ras          = LPSegNet().cuda()
    input_tensor = torch.randn(1, 3, 352, 352).cuda()
    out          = ras(input_tensor)