import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="5"  # specify which GPU(s) to be used

import argparse
import torch
import torch.nn.functional  as F
import numpy                as np
import matplotlib.pyplot    as plt
from lib.PraNet_Res2Net_v10 import LPSegNet
from utils.dataloader   import test_dataset
from metrics import metric_IOU

parser = argparse.ArgumentParser()
parser.add_argument('--testsize', type=int, default=352, help='testing size')
parser.add_argument('--pth_path', type=str, default='./snapshots/KQV_27/80.pth')

# dire = '/home/linamruiz/Polyps/data/TestDataset/ASU-Mayo/'

videos = ['Video1', 'Video2', 'Video3', 'Video4', 'Video5', 'Video6', 'Video7', 'Video8', 'Video9', 'Video10', 'Video11', 'Video12', 'Video13', 'Video14', 'Video15', 'Video16', 'Video17', 'Video18']
gt_name = ['GT_1', 'GT_2', 'GT_3', 'GT_4', 'GT_5', 'GT_6', 'GT_7', 'GT_8', 'GT_9', 'GT_10', 'GT_11', 'GT_12', 'GT_13', 'GT_14', 'GT_15', 'GT_16', 'GT_17', 'GT_18']

f = open ('CVC-KQV25.txt','a')

for index, _data_name in enumerate(videos):
# for _data_name in ['ETIS-LaribPolypDB']:

    # f = open ('IGHO' + _data_name +'_KQV_v10.txt','a')
    # data_path = '/data/Datasets/Igho/Uncompressed/Videos/AGOSTO/{}'.format(_data_name)
    data_path   = '/home/linamruiz/Polyps/data/TestDataset/CVC-Video/images/{}'.format(_data_name)
    gt_path     = '/home/linamruiz/Polyps/data/TestDataset/CVC-Video/masks/' + gt_name[index] + '/'

    save_path = './results/IGHO_v10/{}/'.format(_data_name)
    opt = parser.parse_args()
    
    model = LPSegNet()
    model.load_state_dict(torch.load(opt.pth_path))
    model.cuda()
    model.eval()

    # os.makedirs(save_path, exist_ok=True)
    image_root  = '{}/'.format(data_path)   ##Normal
    # gt_root  = '{}/masks/'.format(data_path)       ##Masks
    gt_root     = gt_path
    test_loader = test_dataset(image_root, gt_root, opt.testsize)
    # value_conv5 = model.agg_LP.conv5.weight.data
    # value_conv5 = value_conv5.detach().cpu().numpy().squeeze()

    f.write( "\n\n--------------------------------------------------------\nGT " + str(index+1) + " \n")
    for i in range(test_loader.size):

        image, gt, name = test_loader.load_data()
        gt = np.asarray(gt, np.float32)
        # label = np.asarray([1.0 , 1.0], np.float32) if gt.max() == 255.0 else np.asarray([1.0 , 0.0], np.float32)
        label = 1 if gt.max() == 255.0 else 0
        gt /= (gt.max() + 1e-8)
        image      = image.cuda()
        map_seg, c = model(image)
    
        res = map_seg
        res = F.interpolate(res, size=gt.shape, mode='bilinear', align_corners=False)
        res = torch.round(res.sigmoid())
        res = res.data.cpu().numpy().squeeze()
        res = (res - res.min()) / (res.max() - res.min() + 1e-8)
        f.write(str(round(metric_IOU(res, gt, label)*100,2)) + "\n")
        # f.write(str(label) + "\n")
        # print(round(metric_IOU(res, gt, label)*100,2))
        # plt.imsave(save_path+name, res, cmap='gray')
    # f.close()