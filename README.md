# Weakly Supervised Polyp Segmentation from an Attention Mechanism
## Authors: Lina Ruiz and Fabio Martinez 

This repository provides the code of "Weakly Supervised Polyp Segmentation from an Attention Mechanism" published in EMBC-2022
