import numpy as np
import torch

def metric_IOU(pred, mask, label):
    
    # mask = np.round(mask)
    predict_sum = np.sum(pred)
    if label == 0 and predict_sum == 0.0:  #1479600.0 video 68  ; 1490400.0 video 2
        wiou = 1.0
    else:
        inter = np.sum((pred * mask))
        union = np.sum((pred + mask))
        wiou = (inter + 1 )/(union - inter + 1)

    return  wiou

def calculated_iou(pred, mask): 

    pred = torch.sigmoid(pred)

    pred_ = torch.round(pred)
    mask_ = torch.round(mask)
    
    matrix_iou = torch.cat((pred_, mask_), dim=0)
    matrix = torch.sum(matrix_iou, dim = 0)


    if torch.sum(pred_) == 0 and torch.sum(mask_) == 0:
        iou = torch.tensor(1.0)
    else: 
        inter = torch.sum((matrix == 2.0))
        union = torch.sum((matrix != 0.0))
        iou = inter /union 
    return iou